package message

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestDeserializeMessageHeader(t *testing.T) {
	data := []byte{
		// id
		0x00, 0x01,
		// 1 - QR
		//  0000 - OPCODE
		//      0 - AA
		//       0 - TC
		//        0 - RD
		//         1 - RA
		//          0000 - Z
		//              0000 - RCODE
		0b10000000, 0b10000000,
		// QDCOUNT
		0x00, 0x01,
		// ANCOUNT
		0x00, 0x02,
		// NSCOUNT
		0x00, 0x00,
		// ARCOUNt
		0x00, 0x00,
	}

	expected := &MessageHeader{
		Id:                    0x01,
		IsResponse:            true,
		OpCode:                OpCodeStandardQuery,
		IsAuthoritativeAnswer: false,
		IsTruncated:           false,
		IsRecursionDesired:    false,
		IsRecursionAvailable:  true,
		Z:                     0,
		ResponseCode:          ResponseCodeOk,
		QuestionCount:         1,
		AnswerCount:           2,
		NameServerRecordCount: 0,
		AdditionalRecordCount: 0,
	}

	got, _, err := DeserializeMessageHeader(data)
	require.NoError(t, err)

	require.Equal(t, expected, got)
}
