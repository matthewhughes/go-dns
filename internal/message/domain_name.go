package message

import (
	"encoding/binary"
	"errors"
	"fmt"
	"strings"
)

// map of byte offsets to domain names
// https://datatracker.ietf.org/doc/html/rfc1035#section-4.1.4
type namePointerMap map[int]lookupInfo

type lookupInfo struct {
	// the name this label is part of
	name *DomainName
	// the total length of the raw bytes of the lables
	length int
}

const namePointerMask = 0xc0

// Domain name that enforces the format suggested by
// https://datatracker.ietf.org/doc/html/rfc1035#section-2.3.1
type DomainName struct {
	Labels []string
}

func (d *DomainName) Serialize() []byte {
	serialized := []byte{}
	// We assume label length is valid (i.e. not very big)
	for _, label := range d.Labels {
		serialized = append(serialized, byte(len(label)))
		serialized = append(serialized, []byte(label)...)
	}

	// The domain name terminates with the
	// zero length octet for the null label of the root.
	return append(serialized, 0x00)
}

// limits etc. per https://datatracker.ietf.org/doc/html/rfc1035#section-2.3.4
const (
	NameLenLimit       = 255
	LabelLenLimit      = 63
	UDPMessageLenLimit = 512
	LabelDelimiter     = "."
)

func NewDomainName(rawName string) (*DomainName, error) {
	length := 0
	if len(rawName) > 255 {
		return nil, fmt.Errorf("name to long: longer than %d characters", NameLenLimit)
	}

	rawLabels := strings.Split(rawName, LabelDelimiter)
	labels := make([]string, 0, len(rawLabels))
	for _, label := range rawLabels {
		if err := validateLabel([]rune(label)); err != nil {
			return nil, fmt.Errorf("invalid label: %w", err)
		}
		labels = append(labels, label)
		length += len(label)
	}

	return &DomainName{Labels: labels}, nil
}

func (d *DomainName) String() string {
	return strings.Join(d.Labels, LabelDelimiter)
}

func DeserializeDomainName(
	data []byte,
	offset int,
	nameMap namePointerMap,
) (*DomainName, int, error) {
	// See https://datatracker.ietf.org/doc/html/rfc1035#section-4.1.4
	// The pointer takes the form of a two octet sequence:

	// +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	// | 1  1|                OFFSET                   |
	// +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

	// The compression scheme allows a domain name in a message to be
	// represented as either:

	//    - a sequence of labels ending in a zero octet
	//    - a pointer
	//    - a sequence of labels ending with a pointer

	if len(data) == 0 {
		return nil, 0, errors.New("no data to deserialize")
	}
	length := 0
	labels := []string{}
	var name *DomainName
	for i := 0; i < len(data); {
		if data[i] == 0x00 {
			// 1st case above
			name = &DomainName{Labels: labels}
			// add null byte
			length += 1
			break
		}
		if data[i] == byte(namePointerMask) {
			if i > len(data)-2 {
				return nil, 0, errors.New("invalid name pointer: contains no offset")
			}

			// 2nd and 3rd cases
			lookupLabels, err := lookupLabels(binary.BigEndian.Uint16(data[i:i+2]), nameMap)
			if err != nil {
				return nil, 0, err
			}
			length += 2
			name = &DomainName{Labels: append(labels, lookupLabels...)}
			break
		}
		// a regular label
		label, labelLen, err := DeserializeCharacterString(data[i:])
		if err != nil {
			return nil, 0, fmt.Errorf("failed reading label: %v", err)
		}
		if err := validateLabel([]rune(label)); err != nil {
			return nil, 0, fmt.Errorf("invalid label: %v", err)
		}

		labels = append(labels, label)
		length += labelLen
		i += labelLen
	}

	if len(name.Labels) == 0 {
		return nil, 0, fmt.Errorf("no labels")
	}
	// store the name for future lookups
	nameMap[offset] = lookupInfo{name: name, length: length}
	return name, length, nil
}

func lookupLabels(namePointer uint16, nameMap namePointerMap) ([]string, error) {
	// The pointer takes the form of a two octet sequence:

	// 	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	// 	| 1  1|                OFFSET                   |
	// 	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	offset := int(namePointer ^ (namePointerMask << 8))

	for startOffset, info := range nameMap {
		// could the offset be contained in this label?
		if offset >= startOffset && offset < startOffset+info.length {
			runningLen := 0
			for i, label := range info.name.Labels {
				// start of label:
				// startOffset
				// + length of all previous labels
				// + number of previous labels seen (for the '.' byte)
				labelStartOffset := startOffset + runningLen + i
				if labelStartOffset == offset {
					return info.name.Labels[i:], nil
				}
				runningLen += len(label)
			}
		}
	}

	return nil, fmt.Errorf("invalid name pointer: points to an invalid location")
}

func validateLabel(label []rune) error {
	if len(label) == 0 {
		return errors.New("empty label")
	}
	if len(label) > LabelLenLimit {
		return fmt.Errorf("longer than %d characters", LabelLenLimit)
	}

	if !isLetter(label[0]) {
		return errors.New("must start with a letter")
	}

	for i, c := range label {
		if i == len(label)-1 && !(isLetter(c) || isDigit(c)) {
			return errors.New("must end with a letter or digit")
		}
		// ldh-str
		if !(isLetter(c) || isDigit(c) || c == '-') {
			return errors.New("must contain only letters, digits, and hyphens")
		}
	}

	return nil
}

func isLetter(c rune) bool {
	return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')
}

func isDigit(c rune) bool {
	return c >= '0' && c <= '9'
}
