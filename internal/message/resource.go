package message

import (
	"encoding/binary"
	"errors"
	"fmt"
	"net/netip"
)

// classes per https://datatracker.ietf.org/doc/html/rfc1035#section-3.2.4,
// and https://datatracker.ietf.org/doc/html/rfc1035#section-3.2.5
type Class struct {
	Type       string
	Value      uint16
	IsObsolete bool
}

var (
	// CLASS
	ClassInternet = Class{Type: "IN", Value: 1}
	ClassCSNet    = Class{Type: "CS", Value: 2, IsObsolete: true}
	ClassChaos    = Class{Type: "CH", Value: 3}
	ClassHesiod   = Class{Type: "HS", Value: 4}

	// QCLASS
	ClassAny = Class{Type: "*", Value: 255}
)

const (
	hostAddrLength = 4
)

var classes = [...]Class{
	ClassInternet,
	ClassCSNet,
	ClassChaos,
	ClassHesiod,
	ClassAny,
}

// types per https://datatracker.ietf.org/doc/html/rfc1035#section-3.2.2
// and https://datatracker.ietf.org/doc/html/rfc1035#section-3.2.3
type ResourceType struct {
	Type           string
	Value          uint16
	IsObsolete     bool
	IsExperimental bool
}

var (
	// TYPE
	TypeHostAddress                 = ResourceType{Type: "A", Value: 1}
	TypeAuthoritativeNameServer     = ResourceType{Type: "NS", Value: 2}
	TypeMailDestination             = ResourceType{Type: "MD", Value: 3, IsObsolete: true}
	TypeMailForwarder               = ResourceType{Type: "MF", Value: 4, IsObsolete: true}
	TypeCanonicalName               = ResourceType{Type: "CNAME", Value: 5}
	TypeStartOfAuthority            = ResourceType{Type: "SOA", Value: 6}
	TypeMailboxDomainName           = ResourceType{Type: "MB", Value: 7, IsExperimental: true}
	TypeMailGroupMember             = ResourceType{Type: "MG", Value: 8, IsExperimental: true}
	TypeMailRenameDomainName        = ResourceType{Type: "MR", Value: 9, IsExperimental: true}
	TypeNull                        = ResourceType{Type: "NULL", Value: 10, IsExperimental: true}
	TypeWellKnownServiceDescription = ResourceType{Type: "WKS", Value: 11}
	TypeDomainNamePointer           = ResourceType{Type: "PTR", Value: 12}
	TypeHostInformation             = ResourceType{Type: "HINFO", Value: 13}
	TypeMailboxInformation          = ResourceType{Type: "MINFO", Value: 14}
	TypeMailExchange                = ResourceType{Type: "MX", Value: 15}
	TypeText                        = ResourceType{Type: "TXT", Value: 16}

	// QTYPE
	TypeZoneTransfer  = ResourceType{Type: "AXFR", Value: 252}
	TypeMailboxRecord = ResourceType{Type: "MAILB", Value: 253}
	TypeMailAgent     = ResourceType{Type: "MAILA", Value: 254, IsObsolete: true}
	TypeAll           = ResourceType{Type: "*", Value: 255}
)

var resourceTypes = [...]ResourceType{
	TypeHostAddress,
	TypeAuthoritativeNameServer,
	TypeMailDestination,
	TypeMailForwarder,
	TypeCanonicalName,
	TypeStartOfAuthority,
	TypeMailboxDomainName,
	TypeMailGroupMember,
	TypeMailRenameDomainName,
	TypeNull,
	TypeWellKnownServiceDescription,
	TypeDomainNamePointer,
	TypeHostInformation,
	TypeMailboxInformation,
	TypeMailExchange,
	TypeText,
	TypeZoneTransfer,
	TypeMailboxRecord,
	TypeMailAgent,
	TypeAll,
}

const (
	resourceTypeLength   = 2
	resourceClassLength  = 2
	TTLLength            = 4
	ResourceLengthLength = 2
)

/*
ResourceRecord is a resource record per
https://datatracker.ietf.org/doc/html/rfc1035#section-4.1.3

The record contains the following:

	                                1  1  1  1  1  1
	  0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	|                                               |
	/                                               /
	/                      NAME                     /
	|                                               |
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	|                      TYPE                     |
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	|                     CLASS                     |
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	|                      TTL                      |
	|                                               |
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	|                   RDLENGTH                    |
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--|
	/                     RDATA                     /
	/                                               /
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
*/
type ResourceRecord struct {
	Name           *DomainName
	Type           *ResourceType
	Class          *Class
	TTL            uint32
	ResourceLength uint16
	Data           []byte
	Resource       any
}

type HostInfo struct {
	CPU string
	OS  string
}

func DeserializeResourceRecord(
	data []byte,
	offset int,
	nameMap namePointerMap,
) (*ResourceRecord, int, error) {
	domainName, length, err := DeserializeDomainName(data, offset, nameMap)
	if err != nil {
		return nil, 0, fmt.Errorf("failed deserializing name: %v", err)
	}

	localOffset := length
	resourceType, length, err := deserializeResourceType(data[localOffset:])
	if err != nil {
		return nil, 0, fmt.Errorf("failed deserializing resource type: %v", err)
	}
	localOffset += length

	resourceClass, length, err := deserializeResourceClass(data[localOffset:])
	if err != nil {
		return nil, 0, fmt.Errorf("failed deserializing resource class: %v", err)
	}
	localOffset += length

	if localOffset+TTLLength > len(data) {
		return nil, 0, errors.New("unexpected end of answer before TTL")
	}
	ttl := binary.BigEndian.Uint32(data[localOffset:])
	localOffset += TTLLength

	if localOffset+ResourceLengthLength > len(data) {
		return nil, 0, errors.New("unexpected end of answer before resource length")
	}
	resourceLength := binary.BigEndian.Uint16(data[localOffset:])
	localOffset += ResourceLengthLength

	if localOffset+int(resourceLength) > len(data) {
		return nil, 0, fmt.Errorf(
			"not enough resource data: given length was %d but only %d bytes remain",
			resourceLength,
			len(data)-localOffset,
		)
	}
	resourceData := data[localOffset : localOffset+int(resourceLength)]

	var resource any
	switch resourceType.Value {
	case TypeHostInformation.Value:
		r, _, err := deserializeHostInfo(resourceData)
		if err != nil {
			return nil, 0, err
		}
		resource = r
	case TypeHostAddress.Value:
		r, _, err := deserializeHostAddress(resourceData)
		if err != nil {
			return nil, 0, err
		}
		resource = r
	case TypeStartOfAuthority.Value:
		r, err := deserializeStartOfAuthority(resourceData, localOffset+offset, nameMap)
		if err != nil {
			return nil, 0, fmt.Errorf("failed deserializing SOA: %v", err)
		}
		resource = r
	case TypeWellKnownServiceDescription.Value:
		r, err := deserializeWellKnownServiceDescription(resourceData)
		if err != nil {
			return nil, 0, fmt.Errorf("failed deserializing WKS: %v", err)
		}
		resource = r
	case TypeMailboxInformation.Value:
		r, err := deserializeMailboxInfo(resourceData, localOffset+offset, nameMap)
		if err != nil {
			return nil, 0, fmt.Errorf("failed deserializing MINFO: %v", err)
		}
		resource = r
	case TypeMailExchange.Value:
		r, err := deserializeMailExchange(resourceData, localOffset+offset, nameMap)
		if err != nil {
			return nil, 0, fmt.Errorf("failed deserializing MX: %v", err)
		}
		resource = r
	case TypeAuthoritativeNameServer.Value,
		TypeDomainNamePointer.Value,
		TypeMailboxDomainName.Value,
		TypeMailForwarder.Value,
		TypeMailGroupMember.Value,
		TypeCanonicalName.Value,
		TypeMailRenameDomainName.Value:
		r, _, err := DeserializeDomainName(resourceData, localOffset+offset, nameMap)
		if err != nil {
			return nil, 0, err
		}
		resource = r
	}

	localOffset += int(resourceLength)

	return &ResourceRecord{
		Name:           domainName,
		Type:           resourceType,
		Class:          resourceClass,
		TTL:            ttl,
		ResourceLength: resourceLength,
		Data:           resourceData,
		Resource:       resource,
	}, localOffset, nil
}

func deserializeResourceType(data []byte) (*ResourceType, int, error) {
	if len(data) < resourceTypeLength {
		return nil, 0, fmt.Errorf("too short: must be exactly %d bytes", resourceTypeLength)
	}
	rawTypeValue := binary.BigEndian.Uint16(data[:resourceTypeLength])

	for _, t := range resourceTypes {
		if t.Value == rawTypeValue {
			return &t, resourceTypeLength, nil
		}
	}

	return nil, resourceTypeLength, fmt.Errorf(
		"no resource type exists with value %d",
		rawTypeValue,
	)
}

func deserializeResourceClass(data []byte) (*Class, int, error) {
	if len(data) < resourceTypeLength {
		return nil, 0, fmt.Errorf("too short: must be exactly %d bytes", resourceTypeLength)
	}
	rawClassValue := binary.BigEndian.Uint16(data[:resourceClassLength])

	for _, c := range classes {
		if c.Value == rawClassValue {
			return &c, resourceClassLength, nil
		}
	}

	return nil, 0, fmt.Errorf("no resource class exists with value %d", rawClassValue)
}

func deserializeHostInfo(data []byte) (*HostInfo, int, error) {
	length := 0
	cpu, CPULength, err := DeserializeCharacterString(data)
	if err != nil {
		return nil, 0, fmt.Errorf("failed to deserialize HINFO: failed to deserialize CPU: %v", err)
	}
	length += CPULength

	os, OSLength, err := DeserializeCharacterString(data[length:])
	if err != nil {
		return nil, 0, fmt.Errorf("failed to deserialize HINFO: failed to deserialize OS: %v", err)
	}
	length += OSLength

	return &HostInfo{CPU: cpu, OS: os}, length, nil
}

func deserializeHostAddress(data []byte) (netip.Addr, int, error) {
	if len(data) != hostAddrLength {
		return netip.IPv4Unspecified(), 0, fmt.Errorf(
			"invalid data for host address: have %d bytes but must be exactly %d bytes",
			len(data),
			hostAddrLength,
		)
	}
	addr, _ := netip.AddrFromSlice(data)

	return addr, hostAddrLength, nil
}

// classes per https://datatracker.ietf.org/doc/html/rfc1035#section-3.3.13,
type StartOfAuthority struct {
	MainName        *DomainName
	ResponsibleName *DomainName
	Serial          uint32
	Refresh         uint32
	Retry           uint32
	Expire          uint32
	Minimum         uint32
}

func deserializeStartOfAuthority(
	data []byte,
	offset int,
	nameMap namePointerMap,
) (*StartOfAuthority, error) {
	mainName, localOffset, err := DeserializeDomainName(data, offset, nameMap)
	if err != nil {
		return nil, fmt.Errorf("failed deserializing main domain name: %v", err)
	}

	respName, length, err := DeserializeDomainName(
		data[localOffset:],
		offset+localOffset,
		nameMap,
	)
	if err != nil {
		return nil, fmt.Errorf("failed deserializing responsible domain name: %v", err)
	}
	localOffset += length

	serial, err := readUint32(data[localOffset:])
	if err != nil {
		return nil, fmt.Errorf("failed deserializing serial: %v", err)
	}
	localOffset += 4

	refresh, err := readUint32(data[localOffset:])
	if err != nil {
		return nil, fmt.Errorf("failed derefreshizing refresh: %v", err)
	}
	localOffset += 4

	retry, err := readUint32(data[localOffset:])
	if err != nil {
		return nil, fmt.Errorf("failed deretryizing retry: %v", err)
	}
	localOffset += 4

	expire, err := readUint32(data[localOffset:])
	if err != nil {
		return nil, fmt.Errorf("failed deexpireizing expire: %v", err)
	}
	localOffset += 4

	minimum, err := readUint32(data[localOffset:])
	if err != nil {
		return nil, fmt.Errorf("failed deminimumizing minimum: %v", err)
	}

	return &StartOfAuthority{
		MainName:        mainName,
		ResponsibleName: respName,
		Serial:          serial,
		Refresh:         refresh,
		Retry:           retry,
		Expire:          expire,
		Minimum:         minimum,
	}, nil
}

func readUint32(data []byte) (uint32, error) {
	requiredLen := 4
	if len(data) < requiredLen {
		return 0, fmt.Errorf(
			"Unexpected end of data, need %d bytes but only have %d",
			requiredLen,
			len(data),
		)
	}

	return binary.BigEndian.Uint32(data), nil
}

// classes per https://datatracker.ietf.org/doc/html/rfc1035#section-3.4.2,
type WellKnownServiceDescription struct {
	Address  netip.Addr
	Protocol uint8
	BitMap   []byte
}

func deserializeWellKnownServiceDescription(data []byte) (*WellKnownServiceDescription, error) {
	addrLength := 4
	if len(data) < addrLength {
		return nil, fmt.Errorf(
			"failed reading address: need %d bytes but only have %d",
			addrLength,
			len(data),
		)
	}
	addr, _ := netip.AddrFromSlice(data[:addrLength])
	offset := addrLength

	protocolLength := 1
	if len(data[offset:]) < protocolLength {
		return nil, fmt.Errorf(
			"failed reading protocol: need %d bytes but only have %d",
			protocolLength,
			len(data[offset:]),
		)
	}
	protocol := data[offset]
	offset += protocolLength

	return &WellKnownServiceDescription{
		Address:  addr,
		Protocol: protocol,
		BitMap:   data[offset:],
	}, nil
}

type MailboxInfo struct {
	ResponsibleMailbox *DomainName
	ErrorMailBox       *DomainName
}

func deserializeMailboxInfo(data []byte, offset int, nameMap namePointerMap) (*MailboxInfo, error) {
	rmailbx, length, err := DeserializeDomainName(data, offset, nameMap)
	if err != nil {
		return nil, fmt.Errorf("failed deserializing RMAILBX: %v", err)
	}

	emailbx, _, err := DeserializeDomainName(data[length:], offset+length, nameMap)
	if err != nil {
		return nil, fmt.Errorf("failed deserializing EMAILBX: %v", err)
	}

	return &MailboxInfo{
		ResponsibleMailbox: rmailbx,
		ErrorMailBox:       emailbx,
	}, nil
}

// classes per https://datatracker.ietf.org/doc/html/rfc1035#section-3.3.9,
type MailExchange struct {
	Priority uint16
	Exchange *DomainName
}

func deserializeMailExchange(
	data []byte,
	offset int,
	nameMap namePointerMap,
) (*MailExchange, error) {
	priorityLength := 2
	if len(data) < priorityLength {
		return nil, fmt.Errorf("failed deserializing priority: need %d bytes but only have %d",
			priorityLength,
			len(data),
		)
	}
	priority := binary.BigEndian.Uint16(data[:priorityLength])

	exchange, _, err := DeserializeDomainName(data[priorityLength:], offset, nameMap)
	if err != nil {
		return nil, fmt.Errorf("failed deserializing exchange: %v", err)
	}

	return &MailExchange{Priority: priority, Exchange: exchange}, nil
}
