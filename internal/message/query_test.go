package message

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestQuery_NewQuery_ErrorsOnBadName(t *testing.T) {
	for _, tc := range []struct {
		name        string
		expectedErr string
	}{
		{
			strings.Repeat("a", 256),
			"name to long: longer than 255 characters",
		},
		{
			strings.Repeat("a", 64) + ".com",
			"invalid label: longer than 63 characters",
		},
		{
			"",
			"invalid label: empty label",
		},
		{
			"-wat.com",
			"invalid label: must start with a letter",
		},
		{
			"1wat.com",
			"invalid label: must start with a letter",
		},
		{
			"&wat.com",
			"invalid label: must start with a letter",
		},
		{
			"wat.c&om",
			"invalid label: must contain only letters, digits, and hyphens",
		},
		{
			"wat.com-",
			"invalid label: must end with a letter or digit",
		},
	} {
		t.Run(fmt.Sprintf("%s:%s", tc.name, tc.expectedErr), func(t *testing.T) {
			_, err := NewQuery(tc.name, TypeAuthoritativeNameServer, ClassInternet)
			require.EqualError(t, err, tc.expectedErr)
		})
	}
}

func TestQuery_Serialize(t *testing.T) {
	query, err := NewQuery("example.com", TypeAuthoritativeNameServer, ClassInternet)
	require.NoError(t, err)

	expected := []byte{
		// ID
		0x00, 0x01,
		// 0 - QR
		//  0000 - OPCODE
		//      0 - AA
		//       0 - TC
		//        0 - RD
		//         0 - RA
		//          0000 - Z
		//              0000 - RCODE
		0b00000000, 0o00000000,
		// QDCOUNT
		0x00, 0x01,
		// ANCOUNT
		0x00, 0x00,
		// NSCOUNT
		0x00, 0x00,
		// ARCOUNT
		0x00, 0x00,
		// QNAME
		0x07, 'e', 'x', 'a', 'm', 'p', 'l', 'e',
		0x03, 'c', 'o', 'm', 0x00,
		// QTYPE
		0x00, 0x02,
		// QCLASS
		0x00, 0x01,
	}

	require.Equal(t, expected, query.Serialize())
}
