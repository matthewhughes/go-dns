package message

import (
	"encoding/binary"
	"fmt"
)

// Question is a single entry in a query as per
// https://datatracker.ietf.org/doc/html/rfc1035#section-4.1.2
type Question struct {
	Name  *DomainName
	Type  *ResourceType
	Class *Class
}

/*
Query is a collection of [Question] as per

https://datatracker.ietf.org/doc/html/rfc1035#section-4.1.2

The question section is used to carry the "question" in most queries,
i.e., the parameters that define what is being asked.  The section
contains QDCOUNT (usually 1) entries, each of the following format:

	                                1  1  1  1  1  1
	  0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	|                                               |
	/                     QNAME                     /
	/                                               /
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	|                     QTYPE                     |
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	|                     QCLASS                    |
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
*/
type Query struct {
	Questions []*Question
}

func (q *Query) BuildHeader() *MessageHeader {
	return &MessageHeader{
		Id:                 1,
		OpCode:             OpCodeStandardQuery,
		IsRecursionDesired: false,
		QuestionCount:      uint16(len(q.Questions)),
	}
}

func (q *Query) Serialize() []byte {
	serialized := q.BuildHeader().Serialize()
	for _, question := range q.Questions {
		serialized = append(serialized, question.Serialize()...)
	}
	return serialized
}

// TODO: just before 2.3.2 is some test data (for valid domains)
func NewQuery(name string, typ ResourceType, class Class) (*Query, error) {
	domainName, err := NewDomainName(name)
	if err != nil {
		return nil, err
	}

	return &Query{Questions: []*Question{{Name: domainName, Type: &typ, Class: &class}}}, nil
}

func (q *Question) Serialize() []byte {
	serialized := q.Name.Serialize()
	serialized = binary.BigEndian.AppendUint16(serialized, q.Type.Value)
	serialized = binary.BigEndian.AppendUint16(serialized, q.Class.Value)

	return serialized
}

func DeserializeQuestion(data []byte, offset int, nameMap namePointerMap) (*Question, int, error) {
	name, length, err := DeserializeDomainName(data, offset, nameMap)
	if err != nil {
		return nil, 0, fmt.Errorf("failed deserializing name: %v", err)
	}
	localOffset := length

	resourceType, length, err := deserializeResourceType(data[localOffset:])
	if err != nil {
		return nil, 0, fmt.Errorf("failed deserializing resource type: %v", err)
	}
	localOffset += length

	resourceClass, length, err := deserializeResourceClass(data[localOffset:])
	if err != nil {
		return nil, 0, fmt.Errorf("failed deserializing resource class: %v", err)
	}

	return &Question{
		Name:  name,
		Type:  resourceType,
		Class: resourceClass,
	}, localOffset + length, nil
}
