package message

import (
	"net/netip"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var (
	validHeader = []byte{
		// id
		0x00, 0x01,
		0b10000000, 0b10000000,
		// QDCOUNT
		0x00, 0x01,
		// ANCOUNT
		0x00, 0x02,
		// NSCOUNT
		0x00, 0x00,
		// ARCOUNT
		0x00, 0x00,
	}
	validDomainName = []byte{
		0x07, 'e', 'x', 'a', 'm', 'p', 'l', 'e',
		0x03, 'c', 'o', 'm', 0x00,
	}
	validResourceType  = []byte{0x00, 0x02}
	validResourceClass = []byte{0x00, 0x01}

	validQuestion = append(
		append(
			append(
				validHeader, validDomainName...,
			),
			validResourceType...,
		),
		validResourceClass...,
	)
)

func TestDeserializeMessage_InvalidQuestions(t *testing.T) {
	for _, tc := range []struct {
		desc         string
		expectedErr  string
		questionData []byte
	}{
		{
			"data ends after header",
			"failed to deserialize question #1: failed deserializing name: no data to deserialize",
			[]byte{},
		},
		{
			"truncated name",
			"failed to deserialize question #1: failed deserializing name: failed reading label: character string length of out bounds of data",
			[]byte{0x07, 'e', 'x', 'a', 'm', 'p'},
		},
		{
			"name not staring with a letter",
			"failed to deserialize question #1: failed deserializing name: invalid label: must start with a letter",
			[]byte{
				0x07, '1', 'x', 'a', 'm', 'p', 'l', 'e',
				0x03, 'c', 'o', 'm', 0x00,
			},
		},
		{
			"name has invalid ending",
			"failed to deserialize question #1: failed deserializing name: invalid label: must end with a letter or digit",
			[]byte{
				0x08, 'e', 'x', 'a', 'm', 'p', 'l', 'e', '-', 0x00,
			},
		},
		{
			"name contains invalid character",
			"failed to deserialize question #1: failed deserializing name: invalid label: must contain only letters, digits, and hyphens",
			[]byte{
				0x07, 'e', 'x', 'a', 'm', '-', '@', 'e', 0x00,
			},
		},
		{
			"name contains an empty label",
			"failed to deserialize question #1: failed deserializing name: no labels",
			[]byte{
				0x00,
			},
		},
		{
			"name contains pointer with no value",
			"failed to deserialize question #1: failed deserializing name: invalid name pointer: contains no offset",
			[]byte{
				0xc0,
			},
		},
		{
			"name contains invalid pointer",
			"failed to deserialize question #1: failed deserializing name: invalid name pointer: points to an invalid location",
			[]byte{
				0xc0, 0xff,
			},
		},
		{
			"data ends before resource type",
			"failed to deserialize question #1: failed deserializing resource type: too short: must be exactly 2 bytes",
			validDomainName,
		},
		{
			"invalid resource type",
			"failed to deserialize question #1: failed deserializing resource type: no resource type exists with value 18",
			append(
				validDomainName,
				0x00, 0x12,
			),
		},
		{
			"data ends before resource class",
			"failed to deserialize question #1: failed deserializing resource class: too short: must be exactly 2 bytes",
			append(
				append(
					validDomainName,
					validResourceType...,
				),
				0x10,
			),
		},
		{
			"invalid resource class",
			"failed to deserialize question #1: failed deserializing resource class: no resource class exists with value 18",
			append(
				append(
					validDomainName,
					validResourceType...,
				),
				0x00, 0x12,
			),
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			data := append(validHeader, tc.questionData...)
			_, err := DeserializeMessage(data)

			require.EqualError(t, err, tc.expectedErr)
		})
	}
}

func TestDeserializeMessage_InvalidAnswers(t *testing.T) {
	for _, tc := range []struct {
		desc        string
		expectedErr string
		answerData  []byte
	}{
		{
			"data ends before answers",
			"failed to deserialize answer #1: failed deserializing name: no data to deserialize",
			[]byte{},
		},
		{
			"truncated name",
			"failed to deserialize answer #1: failed deserializing name: failed reading label: character string length of out bounds of data",
			[]byte{0x07, 'e', 'x', 'a', 'm', 'p'},
		},
		{
			"name not staring with a letter",
			"failed to deserialize answer #1: failed deserializing name: invalid label: must start with a letter",
			[]byte{
				0x07, '1', 'x', 'a', 'm', 'p', 'l', 'e',
				0x03, 'c', 'o', 'm', 0x00,
			},
		},
		{
			"name has invalid ending",
			"failed to deserialize answer #1: failed deserializing name: invalid label: must end with a letter or digit",
			[]byte{
				0x08, 'e', 'x', 'a', 'm', 'p', 'l', 'e', '-', 0x00,
			},
		},
		{
			"name contains invalid character",
			"failed to deserialize answer #1: failed deserializing name: invalid label: must contain only letters, digits, and hyphens",
			[]byte{
				0x07, 'e', 'x', 'a', 'm', '-', '@', 'e', 0x00,
			},
		},
		{
			"name contains an empty label",
			"failed to deserialize answer #1: failed deserializing name: no labels",
			[]byte{
				0x00,
			},
		},
		{
			"name contains pointer with no value",
			"failed to deserialize answer #1: failed deserializing name: invalid name pointer: contains no offset",
			[]byte{
				0xc0,
			},
		},
		{
			"name contains invalid pointer",
			"failed to deserialize answer #1: failed deserializing name: invalid name pointer: points to an invalid location",
			[]byte{
				0xc0, 0xff,
			},
		},
		{
			"data ends before resource type",
			"failed to deserialize answer #1: failed deserializing resource type: too short: must be exactly 2 bytes",
			validDomainName,
		},
		{
			"invalid resource type",
			"failed to deserialize answer #1: failed deserializing resource type: no resource type exists with value 18",
			append(
				validDomainName,
				0x00, 0x12,
			),
		},
		{
			"data ends before resource class",
			"failed to deserialize answer #1: failed deserializing resource class: too short: must be exactly 2 bytes",
			append(
				append(
					validDomainName,
					validResourceType...,
				),
				0x10,
			),
		},
		{
			"invalid resource class",
			"failed to deserialize answer #1: failed deserializing resource class: no resource class exists with value 18",
			append(
				append(
					validDomainName,
					validResourceType...,
				),
				0x00, 0x12,
			),
		},
		{
			"data ends before TTL",
			"failed to deserialize answer #1: unexpected end of answer before TTL",
			append(
				append(
					validDomainName,
					validResourceType...,
				),
				validResourceClass...,
			),
		},
		{
			"data ends before TTL",
			"failed to deserialize answer #1: unexpected end of answer before resource length",
			append(
				append(
					append(
						validDomainName,
						validResourceType...,
					),
					validResourceClass...,
				),
				0x00, 0x00, 0x00, 0x00,
			),
		},
		{
			"data ends before TTL",
			"failed to deserialize answer #1: not enough resource data: given length was 8 but only 4 bytes remain",
			append(
				append(
					append(
						validDomainName,
						validResourceType...,
					),
					validResourceClass...,
				),
				// TTL
				0x00, 0x00, 0x00, 0x00,
				// Resource length
				0x00, 0x08,
				0x00, 0x00, 0x00, 0x00,
			),
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			data := append(validQuestion, tc.answerData...)
			_, err := DeserializeMessage(data)

			require.EqualError(t, err, tc.expectedErr)
		})
	}
}

func TestDeserializeMessage_ValidMessages(t *testing.T) {
	data := []byte{
		// HEADER
		// id
		0x00, 0x01,
		// 1 - QR
		//  0000 - OPCODE
		//      0 - AA
		//       0 - TC
		//        0 - RD
		//         1 - RA
		//          0000 - Z
		//              0000 - RCODE
		0b10000000, 0b10000000,
		// QDCOUNT
		0x00, 0x01,
		// ANCOUNT
		0x00, 0x02,
		// NSCOUNT
		0x00, 0x00,
		// ARCOUNT
		0x00, 0x00,

		// QUERY
		// NAME
		0x07, 'e', 'x', 'a', 'm', 'p', 'l', 'e',
		0x03, 'c', 'o', 'm', 0x00,
		// QTYPE
		0x00, 0x02,
		// QCLASS
		0x00, 0x01,

		// ANSWER
		// first
		// NAME	(pointer)
		0xc0, 0x0c,
		// TYPE
		0x00, 0x02,
		// CLASS
		0x00, 0x01,
		// TTL
		0x00, 0x01, 0x38, 0x66,
		// RDLENGTH
		0x00, 0x14,
		// RDATA
		0x01, 'a',
		0x0c, 'i', 'a', 'n', 'a', '-', 's', 'e', 'r', 'v', 'e', 'r', 's',
		0x03, 'n', 'e', 't', 0x00,

		// second
		// NAME (pointer)
		0xc0, 0x0c,
		// TYPE
		0x00, 0x02,
		// CLASS
		0x00, 0x01,
		// TTL
		0x00, 0x01, 0x38, 0x66,
		// RDLENGTH
		0x00, 0x04,
		// RDATA
		0x01, 0x62, 0xc0, 0x2b,
	}

	expected := &Message{
		Header: &MessageHeader{
			Id:                    0x01,
			IsResponse:            true,
			OpCode:                OpCodeStandardQuery,
			IsAuthoritativeAnswer: false,
			IsTruncated:           false,
			IsRecursionDesired:    false,
			IsRecursionAvailable:  true,
			Z:                     0,
			ResponseCode:          ResponseCodeOk,
			QuestionCount:         1,
			AnswerCount:           2,
			NameServerRecordCount: 0,
			AdditionalRecordCount: 0,
		},
		Query: &Query{
			Questions: []*Question{
				{
					Name:  &DomainName{Labels: []string{"example", "com"}},
					Type:  &TypeAuthoritativeNameServer,
					Class: &ClassInternet,
				},
			},
		},
		Answers: []*ResourceRecord{
			{
				Name:           &DomainName{Labels: []string{"example", "com"}},
				Type:           &TypeAuthoritativeNameServer,
				Class:          &ClassInternet,
				TTL:            0x00013866,
				ResourceLength: 0x0014,
				Data: []byte{
					0x01, 'a',
					0x0c, 'i', 'a', 'n', 'a', '-', 's', 'e', 'r', 'v', 'e', 'r', 's',
					0x03, 'n', 'e', 't', 0x00,
				},
				Resource: &DomainName{Labels: []string{"a", "iana-servers", "net"}},
			},
			{
				Name:           &DomainName{Labels: []string{"example", "com"}},
				Type:           &TypeAuthoritativeNameServer,
				Class:          &ClassInternet,
				TTL:            0x00013866,
				ResourceLength: 0x0004,
				Data:           []byte{0x01, 'b', 0xc0, 0x2b},
				Resource:       &DomainName{Labels: []string{"b", "iana-servers", "net"}},
			},
		},
	}

	got, err := DeserializeMessage(data)
	require.NoError(t, err)

	require.Equal(t, expected, got)
}

func TestDeserializeMessage_ValidMessage(t *testing.T) {
	data := []byte{
		// HEADER
		// id
		0xcc, 0xe1,
		// 1 - QR
		// 0000 - OPCODE
		//     1 - AA
		//      0 - TC
		//       1 - RD
		//            0 - RA
		//             000 - Z
		//                0000 - RCODE
		0b10000101, 0b00000000,
		// QDCOUNT
		0x00, 0x01,
		// ANCOUNT
		0x00, 0x01,
		// NSCOUNT
		0x00, 0x00,
		// ARCOUNT
		0x00, 0x01,

		// QUERY
		// NAME
		0x09, 'w', 'i', 'k', 'i', 'p', 'e', 'd', 'i', 'a',
		0x03, 'o', 'r', 'g', 0x00,
		// QTYPE
		0x00, 0xff,
		// QCLASS
		0x00, 0x01,

		// ANSWER
		// NAME (pointer)
		0xc0, 0x0c,
		// TYPE
		0x00, 0x0d,
		// CLASS
		0x00, 0x01,
		// TTL
		0x00, 0x00, 0x0e, 0x10,
		// RDLENGTH
		0x00, 0x09,
		// RDATA
		0x07, 'R', 'F', 'C', '8', '4', '8', '2',
		0x00,
	}
	expected := &Message{
		Header: &MessageHeader{
			Id:                    0xcce1,
			IsResponse:            true,
			OpCode:                OpCodeStandardQuery,
			IsAuthoritativeAnswer: true,
			IsTruncated:           false,
			IsRecursionDesired:    true,
			IsRecursionAvailable:  false,
			Z:                     0,
			ResponseCode:          ResponseCodeOk,
			QuestionCount:         1,
			AnswerCount:           1,
			NameServerRecordCount: 0,
			AdditionalRecordCount: 1,
		},
		Query: &Query{
			Questions: []*Question{
				{
					Name:  &DomainName{Labels: []string{"wikipedia", "org"}},
					Type:  &TypeAll,
					Class: &ClassInternet,
				},
			},
		},
		Answers: []*ResourceRecord{
			{
				Name:           &DomainName{Labels: []string{"wikipedia", "org"}},
				Type:           &TypeHostInformation,
				Class:          &ClassInternet,
				TTL:            0x0e10,
				ResourceLength: 0x09,
				Data:           []byte{0x07, 'R', 'F', 'C', '8', '4', '8', '2', 0x00},
				Resource: &HostInfo{
					CPU: "RFC8482",
					OS:  "",
				},
			},
		},
	}

	got, err := DeserializeMessage(data)
	require.NoError(t, err)

	require.Equal(t, expected.Header, got.Header)
	require.Equal(t, expected.Query, got.Query)
	require.Equal(t, expected.Answers, got.Answers)
}

func TestDeserializeMessage_ValidMessagess(t *testing.T) {
	for _, tc := range []struct {
		data     []byte
		expected *Message
	}{
		{
			[]byte{
				0x36, 0x37,
				//1 - QR
				// 0000 - OPCODE
				//     0 - AA
				//      0 - TC
				//       1 - RD
				//
				0b10000001,
				// 1 - RA
				// 000 - Z
				//    0000 - RCODE
				0b10000000,
				// QDCOUNT
				0x00, 0x01,
				// ANCOUNT
				0x00, 0x01,
				// NSCOUNT
				0x00, 0x00,
				// ARCOUNT
				0x00, 0x00,

				// QUERIES
				// QUESTION
				// NAME
				0x01, 'A',
				0x03, 'd', 'n', 's',
				0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
				0x03, 'o', 'r', 'g',
				0x00,
				// QTYPE
				0x00, 0x01,
				// QCLASS
				0x00, 0x01,

				// ANSWER
				// NAME (pointer)
				0xc0, 0x0c,
				// TYPE
				0x00, 0x01,
				// CLASS
				0x00, 0x01,
				// TTL
				0x00, 0x00, 0x0e, 0x10,
				// RDLENGTH
				0x00, 0x04,
				// RDATA
				0xa6, 0x54, 0x07, 0x63,
			},
			&Message{
				Header: &MessageHeader{
					Id:                    0x3637,
					IsResponse:            true,
					OpCode:                OpCodeStandardQuery,
					IsAuthoritativeAnswer: false,
					IsTruncated:           false,
					IsRecursionDesired:    true,
					IsRecursionAvailable:  true,
					Z:                     0,
					ResponseCode:          ResponseCodeOk,
					QuestionCount:         1,
					AnswerCount:           1,
					NameServerRecordCount: 0,
					AdditionalRecordCount: 0,
				},
				Query: &Query{
					Questions: []*Question{
						{
							Name: &DomainName{Labels: []string{
								"A",
								"dns",
								"netmeister",
								"org",
							}},
							Type:  &TypeHostAddress,
							Class: &ClassInternet,
						},
					},
				},
				Answers: []*ResourceRecord{
					{
						Name: &DomainName{Labels: []string{
							"A",
							"dns",
							"netmeister",
							"org",
						}},
						Type:           &TypeHostAddress,
						Class:          &ClassInternet,
						TTL:            0x0e10,
						ResourceLength: 4,
						Data:           []byte{0xa6, 0x54, 0x07, 0x63},
						Resource:       netip.AddrFrom4([...]byte{0xa6, 0x54, 0x07, 0x63}),
					},
				},
			},
		},
		{
			[]byte{
				0x36, 0x37,
				//1 - qr
				// 0000 - opcode
				//     0 - aa
				//      0 - tc
				//       1 - rd
				//
				0b10000001,
				// 1 - ra
				// 000 - z
				//    0000 - rcode
				0b10000000,
				// QDCOUNT
				0x00, 0x01,
				// ANCOUNT
				0x00, 0x01,
				// NSCOUNT
				0x00, 0x00,
				// ARCOUNT
				0x00, 0x00,

				// QUERIES
				// QUESTION
				// NAME
				0x02, 'N', 'S',
				0x03, 'd', 'n', 's',
				0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
				0x03, 'o', 'r', 'g',
				0x00,
				// QTYPE
				0x00, 0x02,
				// QCLASS
				0x00, 0x01,

				// ANSWER
				// NAME (pointer)
				0xc0, 0x0c,
				// TYPE
				0x00, 0x02,
				// CLASS
				0x00, 0x01,
				// TTL
				0x00, 0x00, 0x0e, 0x10,
				// RDLENGTH
				0x00, 0x08,
				// RDATA
				0x05, 'p', 'a', 'n', 'i', 'x',
				0xc0, 0x13,
			},
			&Message{
				Header: &MessageHeader{
					Id:                    0x3637,
					IsResponse:            true,
					OpCode:                OpCodeStandardQuery,
					IsAuthoritativeAnswer: false,
					IsTruncated:           false,
					IsRecursionDesired:    true,
					IsRecursionAvailable:  true,
					Z:                     0,
					ResponseCode:          ResponseCodeOk,
					QuestionCount:         1,
					AnswerCount:           1,
					NameServerRecordCount: 0,
					AdditionalRecordCount: 0,
				},
				Query: &Query{
					Questions: []*Question{
						{
							Name: &DomainName{Labels: []string{
								"NS",
								"dns",
								"netmeister",
								"org",
							}},
							Type:  &TypeAuthoritativeNameServer,
							Class: &ClassInternet,
						},
					},
				},
				Answers: []*ResourceRecord{
					{
						Name: &DomainName{Labels: []string{
							"NS",
							"dns",
							"netmeister",
							"org",
						}},
						Type:           &TypeAuthoritativeNameServer,
						Class:          &ClassInternet,
						TTL:            0x0e10,
						ResourceLength: 8,
						Data: []byte{
							0x05, 'p', 'a', 'n', 'i', 'x',
							0xc0, 0x13,
						},
						Resource: &DomainName{Labels: []string{
							"panix",
							"netmeister",
							"org",
						}},
					},
				},
			},
		},
		{
			[]byte{
				0x23, 0xf6,
				//1 - QR
				// 0000 - OPCODE
				//     0 - AA
				//      0 - TC
				//       1 - RD
				//
				0b10000001,
				// 1 - RA
				// 000 - Z
				//    0000 - RCODE
				0b10000000,
				// QDCOUNT
				0x00, 0x01,
				// ANCOUNT
				0x00, 0x00,
				// NSCOUNT
				0x00, 0x01,
				// ARCOUNT
				0x00, 0x00,

				// QUERIES
				// QUESTION
				0x02, 'M', 'D',
				0x03, 'd', 'n', 's',
				0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
				0x03, 'o', 'r', 'g',
				0x00,
				// QTYPE
				0x00, 0x03,
				// QCLASS
				0x00, 0x01,
			},
			&Message{
				Header: &MessageHeader{
					Id:                    0x23f6,
					IsResponse:            true,
					OpCode:                OpCodeStandardQuery,
					IsAuthoritativeAnswer: false,
					IsTruncated:           false,
					IsRecursionDesired:    true,
					IsRecursionAvailable:  true,
					Z:                     0,
					ResponseCode:          ResponseCodeOk,
					QuestionCount:         1,
					AnswerCount:           0,
					NameServerRecordCount: 1,
					AdditionalRecordCount: 0,
				},
				Query: &Query{
					Questions: []*Question{
						{
							Name: &DomainName{Labels: []string{
								"MD",
								"dns",
								"netmeister",
								"org",
							}},
							Type:  &TypeMailDestination,
							Class: &ClassInternet,
						},
					},
				},
				Answers: []*ResourceRecord{},
			},
		},
		{
			[]byte{
				0x42, 0xa3,
				//1 - QR
				// 0000 - OPCODE
				//     0 - AA
				//      0 - TC
				//       1 - RD
				//
				0b10000001,
				// 1 - RA
				// 000 - Z
				//    0000 - RCODE
				0b10000000,
				// QDCOUNT
				0x00, 0x01,
				// ANCOUNT
				0x00, 0x00,
				// NSCOuNT
				0x00, 0x01,
				// ARCOUNT
				0x00, 0x00,

				// QUERIES
				// QUESTION
				0x02, 'M', 'F',
				0x03, 'd', 'n', 's',
				0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
				0x03, 'o', 'r', 'g',
				0x00,
				// QTYPE
				0x00, 0x04,
				// QCLASS
				0x00, 0x01,
			},
			&Message{
				Header: &MessageHeader{
					Id:                    0x42a3,
					IsResponse:            true,
					OpCode:                OpCodeStandardQuery,
					IsAuthoritativeAnswer: false,
					IsTruncated:           false,
					IsRecursionDesired:    true,
					IsRecursionAvailable:  true,
					Z:                     0,
					ResponseCode:          ResponseCodeOk,
					QuestionCount:         1,
					AnswerCount:           0,
					NameServerRecordCount: 1,
					AdditionalRecordCount: 0,
				},
				Query: &Query{
					Questions: []*Question{
						{
							Name: &DomainName{Labels: []string{
								"MF",
								"dns",
								"netmeister",
								"org",
							}},
							Type:  &TypeMailForwarder,
							Class: &ClassInternet,
						},
					},
				},
				Answers: []*ResourceRecord{},
			},
		},
		{
			[]byte{
				0x28, 0x3b,
				//1 - QR
				// 0000 - OPCODE
				//     0 - AA
				//      0 - TC
				//       1 - RD
				//
				0b10000001,
				// 1 - RA
				// 000 - Z
				//    0000 - RCODE
				0b10000000,
				// QDCOUNT
				0x00, 0x01,
				// ANCOUNT
				0x00, 0x01,
				// NSCOUNT
				0x00, 0x00,
				// ARCOUNT
				0x00, 0x00,

				// QUESTION
				// QUERIES
				0x05, 'C', 'N', 'A', 'M', 'E',
				0x03, 'd', 'n', 's',
				0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
				0x03, 'o', 'r', 'g',
				0x00,
				// QTYPE
				0x00, 0x05,
				// QCLASS
				0x00, 0x01,

				// ANSWER
				// NAME (pointer)
				0xc0, 0x0c,
				// TYPE
				0x00, 0x05,
				// CLASS
				0x00, 0x01,
				// TTL
				0x00, 0x00, 0x0e, 0x10,
				// RDLENGTH
				0x00, 0x0c,
				// RDATA
				0x09, 'c', 'n', 'a', 'm', 'e', '-', 't', 'x', 't',
				0xc0, 0x12,
			},
			&Message{
				Header: &MessageHeader{
					Id:                    0x283b,
					IsResponse:            true,
					OpCode:                OpCodeStandardQuery,
					IsAuthoritativeAnswer: false,
					IsTruncated:           false,
					IsRecursionDesired:    true,
					IsRecursionAvailable:  true,
					Z:                     0,
					ResponseCode:          ResponseCodeOk,
					QuestionCount:         1,
					AnswerCount:           1,
					NameServerRecordCount: 0,
					AdditionalRecordCount: 0,
				},
				Query: &Query{
					Questions: []*Question{
						{
							Name: &DomainName{Labels: []string{
								"CNAME",
								"dns",
								"netmeister",
								"org",
							}},
							Type:  &TypeCanonicalName,
							Class: &ClassInternet,
						},
					},
				},
				Answers: []*ResourceRecord{
					{
						Name: &DomainName{Labels: []string{
							"CNAME",
							"dns",
							"netmeister",
							"org",
						}},
						Type:  &TypeCanonicalName,
						Class: &ClassInternet,
						TTL:   0x0e10,
						Data: []byte{
							0x09, 'c', 'n', 'a', 'm', 'e', '-', 't', 'x', 't',
							0xc0, 0x12,
						},
						ResourceLength: 0x0c,
						Resource: &DomainName{
							Labels: []string{
								"cname-txt",
								"dns",
								"netmeister",
								"org",
							},
						},
					},
				},
			},
		},
		{
			[]byte{
				0x63, 0x5a,
				//1 - QR
				// 0000 - OPCODE
				//     0 - AA
				//      0 - TC
				//       1 - RD
				//
				0b10000001,
				// 1 - RA
				// 000 - Z
				//    0000 - RCODE
				0b10000000,
				// QDCOUNT
				0x00, 0x01,
				// ANCOUNT
				0x00, 0x01,
				// NSCOUNT
				0x00, 0x00,
				// ARCOUNT
				0x00, 0x00,

				// QUESTION
				// QUERIES
				0x03, 'S', 'O', 'A',
				0x03, 'd', 'n', 's',
				0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
				0x03, 'o', 'r', 'g',
				0x00,
				// QTYPE
				0x00, 0x06,
				// QCLASS
				0x00, 0x01,

				// ANSWER
				// NAME (pointer)
				0xc0, 0x0c,
				// TYPE
				0x00, 0x06,
				// CLASS
				0x00, 0x01,
				// TTL
				0x00, 0x00, 0x0e, 0x10,
				// RDLENGTH
				0x00, 0x27,
				// RDATA
				0x05, 'p', 'a', 'n', 'i', 'x',
				0xc0, 0x14,
				0x08, 'j', 's', 'c', 'h', 'a', 'u', 'm', 'a',
				0xc0, 0x14,
				0x78, 0x77, 0x1e, 0xe3,
				0x00, 0x00, 0x0e, 0x10,
				0x00, 0x00, 0x10, 0x2c,
				0x00, 0x36, 0x33, 0x80,
				0x00, 0x00, 0x0e, 0x10,
			},
			&Message{
				Header: &MessageHeader{
					Id:                    0x635a,
					IsResponse:            true,
					OpCode:                OpCodeStandardQuery,
					IsAuthoritativeAnswer: false,
					IsTruncated:           false,
					IsRecursionDesired:    true,
					IsRecursionAvailable:  true,
					Z:                     0,
					ResponseCode:          ResponseCodeOk,
					QuestionCount:         1,
					AnswerCount:           1,
					NameServerRecordCount: 0,
					AdditionalRecordCount: 0,
				},
				Query: &Query{
					Questions: []*Question{
						{
							Name: &DomainName{Labels: []string{
								"SOA",
								"dns",
								"netmeister",
								"org",
							}},
							Type:  &TypeStartOfAuthority,
							Class: &ClassInternet,
						},
					},
				},
				Answers: []*ResourceRecord{
					{
						Name: &DomainName{Labels: []string{
							"SOA",
							"dns",
							"netmeister",
							"org",
						}},
						Type:           &TypeStartOfAuthority,
						Class:          &ClassInternet,
						TTL:            0x0e10,
						ResourceLength: 0x27,
						Data: []byte{
							0x05, 'p', 'a', 'n', 'i', 'x',
							0xc0, 0x14,
							0x08, 'j', 's', 'c', 'h', 'a', 'u', 'm', 'a',
							0xc0, 0x14,
							0x78, 0x77, 0x1e, 0xe3,
							0x00, 0x00, 0x0e, 0x10,
							0x00, 0x00, 0x10, 0x2c,
							0x00, 0x36, 0x33, 0x80,
							0x00, 0x00, 0x0e, 0x10,
						},
						Resource: &StartOfAuthority{
							MainName: &DomainName{
								Labels: []string{
									"panix",
									"netmeister",
									"org",
								},
							},
							ResponsibleName: &DomainName{
								Labels: []string{
									"jschauma",
									"netmeister",
									"org",
								},
							},
							Serial:  0x78771ee3,
							Refresh: 0x0e10,
							Retry:   0x102c,
							Expire:  0x363380,
							Minimum: 0x0e10,
						},
					},
				},
			},
		},
		{
			[]byte{
				0x04, 0xd0,
				//1 - QR
				// 0000 - OPCODE
				//     0 - AA
				//      0 - TC
				//       1 - RD
				//
				0b10000001,
				// 1 - RA
				// 000 - Z
				//    0000 - RCODE
				0b10000000,
				// QDCOUNT
				0x00, 0x01,
				// ANCOUNT
				0x00, 0x01,
				// NSCOUNT
				0x00, 0x00,
				// ARCOUNT
				0x00, 0x00,

				// QUESTION
				// QUERIES
				0x02, 'M', 'B',
				0x03, 'd', 'n', 's',
				0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
				0x03, 'o', 'r', 'g',
				0x00,
				// QTYPE
				0x00, 0x07,
				// QCLASS
				0x00, 0x01,

				// ANSWER
				// NAME (pointer)
				0xc0, 0x0c,
				// TYPE
				0x00, 0x07,
				// CLASS
				0x00, 0x01,
				// TTL
				0x00, 0x00, 0x0e, 0x10,
				// RDLENGTH
				0x00, 0x16,
				// RDATA
				0x05, 'p', 'a', 'n', 'i', 'x',
				0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
				0x03, 'o', 'r', 'g',
				0x00,
			},
			&Message{
				Header: &MessageHeader{
					Id:                    0x04d0,
					IsResponse:            true,
					OpCode:                OpCodeStandardQuery,
					IsAuthoritativeAnswer: false,
					IsTruncated:           false,
					IsRecursionDesired:    true,
					IsRecursionAvailable:  true,
					Z:                     0,
					ResponseCode:          ResponseCodeOk,
					QuestionCount:         1,
					AnswerCount:           1,
					NameServerRecordCount: 0,
					AdditionalRecordCount: 0,
				},
				Query: &Query{
					Questions: []*Question{
						{
							Name: &DomainName{Labels: []string{
								"MB",
								"dns",
								"netmeister",
								"org",
							}},
							Type:  &TypeMailboxDomainName,
							Class: &ClassInternet,
						},
					},
				},
				Answers: []*ResourceRecord{
					{
						Name: &DomainName{Labels: []string{
							"MB",
							"dns",
							"netmeister",
							"org",
						}},
						Type:           &TypeMailboxDomainName,
						Class:          &ClassInternet,
						TTL:            0x0e10,
						ResourceLength: 0x16,
						Data: []byte{
							0x05, 'p', 'a', 'n', 'i', 'x',
							0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
							0x03, 'o', 'r', 'g',
							0x00,
						},
						Resource: &DomainName{
							Labels: []string{
								"panix",
								"netmeister",
								"org",
							},
						},
					},
				},
			},
		},
		{
			[]byte{
				0x03, 0xfd,
				//1 - QR
				// 0000 - OPCODE
				//     0 - AA
				//      0 - TC
				//       1 - RD
				//
				0b10000001,
				// 1 - RA
				// 000 - Z
				//    0000 - RCODE
				0b10000000,
				// QDCOUNT
				0x00, 0x01,
				// ANCOUNT
				0x00, 0x01,
				// NSCOUNT
				0x00, 0x00,
				// ARCOUNT
				0x00, 0x00,

				// QUESTION
				// QUERIES
				0x02, 'M', 'R',
				0x03, 'd', 'n', 's',
				0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
				0x03, 'o', 'r', 'g',
				0x00,
				// QTYPE
				0x00, 0x09,
				// QCLASS
				0x00, 0x01,

				// ANSWER
				// NAME (pointer)
				0xc0, 0x0c,
				// TYPE
				0x00, 0x09,
				// CLASS
				0x00, 0x01,
				// TTL
				0x00, 0x00, 0x0e, 0x10,
				// RDLENGTH
				0x00, 0x16,
				// RDATA
				0x05, 'p', 'a', 'n', 'i', 'x',
				0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
				0x03, 'o', 'r', 'g',
				0x00,
			},
			&Message{
				Header: &MessageHeader{
					Id:                    0x03fd,
					IsResponse:            true,
					OpCode:                OpCodeStandardQuery,
					IsAuthoritativeAnswer: false,
					IsTruncated:           false,
					IsRecursionDesired:    true,
					IsRecursionAvailable:  true,
					Z:                     0,
					ResponseCode:          ResponseCodeOk,
					QuestionCount:         1,
					AnswerCount:           1,
					NameServerRecordCount: 0,
					AdditionalRecordCount: 0,
				},
				Query: &Query{
					Questions: []*Question{
						{
							Name: &DomainName{Labels: []string{
								"MR",
								"dns",
								"netmeister",
								"org",
							}},
							Type:  &TypeMailRenameDomainName,
							Class: &ClassInternet,
						},
					},
				},
				Answers: []*ResourceRecord{
					{
						Name: &DomainName{Labels: []string{
							"MR",
							"dns",
							"netmeister",
							"org",
						}},
						Type:           &TypeMailRenameDomainName,
						Class:          &ClassInternet,
						TTL:            0x0e10,
						ResourceLength: 0x16,
						Data: []byte{
							0x05, 'p', 'a', 'n', 'i', 'x',
							0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
							0x03, 'o', 'r', 'g',
							0x00,
						},
						Resource: &DomainName{
							Labels: []string{
								"panix",
								"netmeister",
								"org",
							},
						},
					},
				},
			},
		},
		{
			[]byte{
				0x29, 0xd4,
				//1 - QR
				// 0000 - OPCODE
				//     0 - AA
				//      0 - TC
				//       1 - RD
				//
				0b10000001,
				// 1 - RA
				// 000 - Z
				//    0000 - RCODE
				0b10000000,
				// QDCOUNT
				0x00, 0x01,
				// ANCOUNT
				0x00, 0x01,
				// NSCOUNT
				0x00, 0x00,
				// ARCOUNT
				0x00, 0x00,

				// QUESTION
				// QUERIES
				0x03, 'N', 'U', 'L',
				0x03, 'd', 'n', 's',
				0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
				0x03, 'o', 'r', 'g',
				0x00,
				// QTYPE
				0x00, 0x0a,
				// QCLASS
				0x00, 0x01,

				// ANSWER
				// NAME (pointer)
				0xc0, 0x0c,
				// TYPE
				0x00, 0x0a,
				// CLASS
				0x00, 0x01,
				// TTL
				0x00, 0x00, 0x0e, 0x10,
				// RDLENGTH
				0x00, 0x07,
				// RDATA
				'a', 'v', 'o', 'c', 'a', 'd', 'o',
			},
			&Message{
				Header: &MessageHeader{
					Id:                    0x29d4,
					IsResponse:            true,
					OpCode:                OpCodeStandardQuery,
					IsAuthoritativeAnswer: false,
					IsTruncated:           false,
					IsRecursionDesired:    true,
					IsRecursionAvailable:  true,
					Z:                     0,
					ResponseCode:          ResponseCodeOk,
					QuestionCount:         1,
					AnswerCount:           1,
					NameServerRecordCount: 0,
					AdditionalRecordCount: 0,
				},
				Query: &Query{
					Questions: []*Question{
						{
							Name: &DomainName{Labels: []string{
								"NUL",
								"dns",
								"netmeister",
								"org",
							}},
							Type:  &TypeNull,
							Class: &ClassInternet,
						},
					},
				},
				Answers: []*ResourceRecord{
					{
						Name: &DomainName{Labels: []string{
							"NUL",
							"dns",
							"netmeister",
							"org",
						}},
						Type:           &TypeNull,
						Class:          &ClassInternet,
						TTL:            0x0e10,
						ResourceLength: 0x07,
						Data: []byte{
							'a', 'v', 'o', 'c', 'a', 'd', 'o',
						},
						Resource: nil, // data not interpreted for NUL messages
					},
				},
			},
		},
		{
			[]byte{
				0x44, 0xe4,
				//1 - QR
				// 0000 - OPCODE
				//     0 - AA
				//      0 - TC
				//       1 - RD
				//
				0b10000001,
				// 1 - RA
				// 000 - Z
				//    0000 - RCODE
				0b10000000,
				// QDCOUNT
				0x00, 0x01,
				// ANCOUNT
				0x00, 0x02,
				// NSCOUNT
				0x00, 0x00,
				// ARCOUNT
				0x00, 0x00,

				// QUESTION
				// QUERIES
				0x03, 'W', 'K', 'S',
				0x03, 'd', 'n', 's',
				0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
				0x03, 'o', 'r', 'g',
				0x00,
				// QTYPE
				0x00, 0x0b,
				// QCLASS
				0x00, 0x01,

				// ANSWER
				// NAME (pointer)
				0xc0, 0x0c,
				// TYPE
				0x00, 0x0b,
				// CLASS
				0x00, 0x01,
				// TTL
				0x00, 0x00, 0x0e, 0x10,
				// RDLENGTH
				0x00, 0x3d,
				// RDATA
				0xa6, 0x54, 0x07, 0x63,
				0x06,
				0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00,
				0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00,
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10,

				// ANSWER
				// NAME (pointer)
				0xc0, 0x0c,
				// TYPE
				0x00, 0x0b,
				// CLASS
				0x00, 0x01,
				// TTL
				0x00, 0x00, 0x0e, 0x10,
				// RDLENGTH
				0x00, 0x0c,
				// RDATA
				0xa6, 0x54, 0x07, 0x63,
				0x11,
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04,
			},
			&Message{
				Header: &MessageHeader{
					Id:                    0x44e4,
					IsResponse:            true,
					OpCode:                OpCodeStandardQuery,
					IsAuthoritativeAnswer: false,
					IsTruncated:           false,
					IsRecursionDesired:    true,
					IsRecursionAvailable:  true,
					Z:                     0,
					ResponseCode:          ResponseCodeOk,
					QuestionCount:         1,
					AnswerCount:           2,
					NameServerRecordCount: 0,
					AdditionalRecordCount: 0,
				},
				Query: &Query{
					Questions: []*Question{
						{
							Name: &DomainName{Labels: []string{
								"WKS",
								"dns",
								"netmeister",
								"org",
							}},
							Type:  &TypeWellKnownServiceDescription,
							Class: &ClassInternet,
						},
					},
				},
				Answers: []*ResourceRecord{
					{
						Name: &DomainName{Labels: []string{
							"WKS",
							"dns",
							"netmeister",
							"org",
						}},
						Type:           &TypeWellKnownServiceDescription,
						Class:          &ClassInternet,
						TTL:            0x0e10,
						ResourceLength: 0x3d,
						Data: []byte{
							0xa6, 0x54, 0x07, 0x63,
							0x06,
							0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00,
							0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00,
							0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
							0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
							0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
							0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
							0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10,
						},
						Resource: &WellKnownServiceDescription{
							Address:  netip.AddrFrom4([...]byte{0xa6, 0x54, 0x07, 0x63}),
							Protocol: 0x06,
							BitMap: []byte{
								0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00,
								0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00,
								0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
								0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
								0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
								0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
								0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10,
							},
						},
					},
					{
						Name: &DomainName{Labels: []string{
							"WKS",
							"dns",
							"netmeister",
							"org",
						}},
						Type:           &TypeWellKnownServiceDescription,
						Class:          &ClassInternet,
						TTL:            0x0e10,
						ResourceLength: 0x0c,
						Data: []byte{
							0xa6, 0x54, 0x07, 0x63,
							0x11,
							0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04,
						},
						Resource: &WellKnownServiceDescription{
							Address:  netip.AddrFrom4([...]byte{0xa6, 0x54, 0x07, 0x63}),
							Protocol: 0x11,
							BitMap:   []byte{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04},
						},
					},
				},
			},
		},
		{
			[]byte{
				0x73, 0x27,
				//1 - QR
				// 0000 - OPCODE
				//     0 - AA
				//      0 - TC
				//       1 - RD
				//
				0b10000001,
				// 1 - RA
				// 000 - Z
				//    0000 - RCODE
				0b10000000,
				// QDCOUNT
				0x00, 0x01,
				// ANCOUNT
				0x00, 0x01,
				// NSCOUNT
				0x00, 0x00,
				// ARCOUNT
				0x00, 0x00,

				// QUESTION
				// QUERIES
				0x03, 'P', 'T', 'R',
				0x03, 'd', 'n', 's',
				0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
				0x03, 'o', 'r', 'g',
				0x00,
				// QTYPE
				0x00, 0x0c,
				// QCLASS
				0x00, 0x01,

				// ANSWER
				// NAME (pointer)
				0xc0, 0x0c,
				// TYPE
				0x00, 0x0c,
				// CLASS
				0x00, 0x01,
				// TTL
				0x00, 0x00, 0x0e, 0x10,
				// RDLENGTH
				0x00, 0x02,
				// RDATA
				0xc0, 0x0c,
			},
			&Message{
				Header: &MessageHeader{
					Id:                    0x7327,
					IsResponse:            true,
					OpCode:                OpCodeStandardQuery,
					IsAuthoritativeAnswer: false,
					IsTruncated:           false,
					IsRecursionDesired:    true,
					IsRecursionAvailable:  true,
					Z:                     0,
					ResponseCode:          ResponseCodeOk,
					QuestionCount:         1,
					AnswerCount:           1,
					NameServerRecordCount: 0,
					AdditionalRecordCount: 0,
				},
				Query: &Query{
					Questions: []*Question{
						{
							Name: &DomainName{Labels: []string{
								"PTR",
								"dns",
								"netmeister",
								"org",
							}},
							Type:  &TypeDomainNamePointer,
							Class: &ClassInternet,
						},
					},
				},
				Answers: []*ResourceRecord{
					{
						Name: &DomainName{Labels: []string{
							"PTR",
							"dns",
							"netmeister",
							"org",
						}},
						Type:           &TypeDomainNamePointer,
						Class:          &ClassInternet,
						TTL:            0x0e10,
						ResourceLength: 0x02,
						Data: []byte{
							0xc0, 0x0c,
						},
						Resource: &DomainName{
							Labels: []string{
								"PTR",
								"dns",
								"netmeister",
								"org",
							},
						},
					},
				},
			},
		},
		{
			[]byte{
				0x2a, 0x1d,
				//1 - QR
				// 0000 - OPCODE
				//     0 - AA
				//      0 - TC
				//       1 - RD
				//
				0b10000001,
				// 1 - RA
				// 000 - Z
				//    0000 - RCODE
				0b10000000,
				// QDCOUNT
				0x00, 0x01,
				// ANCOUNT
				0x00, 0x01,
				// NSCOUNT
				0x00, 0x00,
				// ARCOUNT
				0x00, 0x00,

				// QUESTION
				// QUERIES
				0x05, 'H', 'I', 'N', 'F', 'O',
				0x03, 'd', 'n', 's',
				0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
				0x03, 'o', 'r', 'g',
				0x00,
				// QTYPE
				0x00, 0x0d,
				// QCLASS
				0x00, 0x01,

				// ANSWER
				// NAME (pointer)
				0xc0, 0x0c,
				// TYPE
				0x00, 0x0d,
				// CLASS
				0x00, 0x01,
				// TTL
				0x00, 0x00, 0x0e, 0x10,
				// RDLENGTH
				0x00, 0x0c,
				// RDATA
				0x06, 'P', 'D', 'P', '-', '1', '1',
				0x04, 'U', 'N', 'I', 'X',
			},
			&Message{
				Header: &MessageHeader{
					Id:                    0x2a1d,
					IsResponse:            true,
					OpCode:                OpCodeStandardQuery,
					IsAuthoritativeAnswer: false,
					IsTruncated:           false,
					IsRecursionDesired:    true,
					IsRecursionAvailable:  true,
					Z:                     0,
					ResponseCode:          ResponseCodeOk,
					QuestionCount:         1,
					AnswerCount:           1,
					NameServerRecordCount: 0,
					AdditionalRecordCount: 0,
				},
				Query: &Query{
					Questions: []*Question{
						{
							Name: &DomainName{Labels: []string{
								"HINFO",
								"dns",
								"netmeister",
								"org",
							}},
							Type:  &TypeHostInformation,
							Class: &ClassInternet,
						},
					},
				},
				Answers: []*ResourceRecord{
					{
						Name: &DomainName{Labels: []string{
							"HINFO",
							"dns",
							"netmeister",
							"org",
						}},
						Type:           &TypeHostInformation,
						Class:          &ClassInternet,
						TTL:            0x0e10,
						ResourceLength: 0x0c,
						Data: []byte{
							0x06, 'P', 'D', 'P', '-', '1', '1',
							0x04, 'U', 'N', 'I', 'X',
						},
						Resource: &HostInfo{
							CPU: "PDP-11",
							OS:  "UNIX",
						},
					},
				},
			},
		},
		{
			[]byte{
				0x88, 0x76,
				//1 - QR
				// 0000 - OPCODE
				//     0 - AA
				//      0 - TC
				//       1 - RD
				//
				0b10000001,
				// 1 - RA
				// 000 - Z
				//    0000 - RCODE
				0b10000000,
				// QDCOUNT
				0x00, 0x01,
				// ANCOUNT
				0x00, 0x01,
				// NSCOUNT
				0x00, 0x00,
				// ARCOUNT
				0x00, 0x00,

				// QUESTION
				// QUERIES
				0x05, 'M', 'I', 'N', 'F', 'O',
				0x03, 'd', 'n', 's',
				0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
				0x03, 'o', 'r', 'g',
				0x00,
				// QTYPE
				0x00, 0x0e,
				// QCLASS
				0x00, 0x01,

				// ANSWER
				// NAME (pointer)
				0xc0, 0x0c,
				// TYPE
				0x00, 0x0e,
				// CLASS
				0x00, 0x01,
				// TTL
				0x00, 0x00, 0x0e, 0x10,
				// RDLENGTH
				0x00, 0x18,
				// RDATA
				0x08, 'j', 's', 'c', 'h', 'a', 'u', 'm', 'a',
				0xc0, 0x16,
				0x0a, 'p', 'o', 's', 't', 'm', 'a', 's', 't', 'e', 'r',
				0xc0, 0x16,
			},
			&Message{
				Header: &MessageHeader{
					Id:                    0x8876,
					IsResponse:            true,
					OpCode:                OpCodeStandardQuery,
					IsAuthoritativeAnswer: false,
					IsTruncated:           false,
					IsRecursionDesired:    true,
					IsRecursionAvailable:  true,
					Z:                     0,
					ResponseCode:          ResponseCodeOk,
					QuestionCount:         1,
					AnswerCount:           1,
					NameServerRecordCount: 0,
					AdditionalRecordCount: 0,
				},
				Query: &Query{
					Questions: []*Question{
						{
							Name: &DomainName{Labels: []string{
								"MINFO",
								"dns",
								"netmeister",
								"org",
							}},
							Type:  &TypeMailboxInformation,
							Class: &ClassInternet,
						},
					},
				},
				Answers: []*ResourceRecord{
					{
						Name: &DomainName{Labels: []string{
							"MINFO",
							"dns",
							"netmeister",
							"org",
						}},
						Type:           &TypeMailboxInformation,
						Class:          &ClassInternet,
						TTL:            0x0e10,
						ResourceLength: 0x18,
						Data: []byte{
							0x08, 'j', 's', 'c', 'h', 'a', 'u', 'm', 'a',
							0xc0, 0x16,
							0x0a, 'p', 'o', 's', 't', 'm', 'a', 's', 't', 'e', 'r',
							0xc0, 0x16,
						},
						Resource: &MailboxInfo{
							ResponsibleMailbox: &DomainName{
								Labels: []string{
									"jschauma",
									"netmeister",
									"org",
								},
							},
							ErrorMailBox: &DomainName{
								Labels: []string{
									"postmaster",
									"netmeister",
									"org",
								},
							},
						},
					},
				},
			},
		},
		{
			[]byte{
				0xda, 0x2d,
				//1 - QR
				// 0000 - OPCODE
				//     0 - AA
				//      0 - TC
				//       1 - RD
				//
				0b10000001,
				// 1 - RA
				// 000 - Z
				//    0000 - RCODE
				0b10000000,
				// QDCOUNT
				0x00, 0x01,
				// ANCOUNT
				0x00, 0x01,
				// NSCOUNT
				0x00, 0x00,
				// ARCOUNT
				0x00, 0x00,

				// QUESTION
				// QUERIES
				0x02, 'M', 'X',
				0x03, 'd', 'n', 's',
				0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
				0x03, 'o', 'r', 'g',
				0x00,
				// QTYPE
				0x00, 0x0f,
				// QCLASS
				0x00, 0x01,

				// ANSWER
				// NAME (pointer)
				0xc0, 0x0c,
				// TYPE
				0x00, 0x0f,
				// CLASS
				0x00, 0x01,
				// TTL
				0x00, 0x00, 0x0e, 0x10,
				// RDLENGTH
				0x00, 0x0a,
				// RDATA
				0x00, 0x32,
				0x05, 'p', 'a', 'n', 'i', 'x',
				0xc0, 0x13,
			},
			&Message{
				Header: &MessageHeader{
					Id:                    0xda2d,
					IsResponse:            true,
					OpCode:                OpCodeStandardQuery,
					IsAuthoritativeAnswer: false,
					IsTruncated:           false,
					IsRecursionDesired:    true,
					IsRecursionAvailable:  true,
					Z:                     0,
					ResponseCode:          ResponseCodeOk,
					QuestionCount:         1,
					AnswerCount:           1,
					NameServerRecordCount: 0,
					AdditionalRecordCount: 0,
				},
				Query: &Query{
					Questions: []*Question{
						{
							Name: &DomainName{Labels: []string{
								"MX",
								"dns",
								"netmeister",
								"org",
							}},
							Type:  &TypeMailExchange,
							Class: &ClassInternet,
						},
					},
				},
				Answers: []*ResourceRecord{
					{
						Name: &DomainName{Labels: []string{
							"MX",
							"dns",
							"netmeister",
							"org",
						}},
						Type:           &TypeMailExchange,
						Class:          &ClassInternet,
						TTL:            0x0e10,
						ResourceLength: 0x0a,
						Data: []byte{
							0x00, 0x32,
							0x05, 'p', 'a', 'n', 'i', 'x',
							0xc0, 0x13,
						},
						Resource: &MailExchange{
							Priority: 0x32,
							Exchange: &DomainName{
								Labels: []string{
									"panix",
									"netmeister",
									"org",
								},
							},
						},
					},
				},
			},
		},
		{
			[]byte{
				0x22, 0x7f,
				//1 - QR
				// 0000 - OPCODE
				//     0 - AA
				//      0 - TC
				//       1 - RD
				//
				0b10000001,
				// 1 - RA
				// 000 - Z
				//    0000 - RCODE
				0b10000000,
				// QDCOUNT
				0x00, 0x01,
				// ANCOUNT
				0x00, 0x02,
				// NSCOUNT
				0x00, 0x00,
				// ARCOUNT
				0x00, 0x00,

				// QUESTION
				// QUERIES
				0x03, 'T', 'X', 'T',
				0x03, 'd', 'n', 's',
				0x0a, 'n', 'e', 't', 'm', 'e', 'i', 's', 't', 'e', 'r',
				0x03, 'o', 'r', 'g',
				0x00,
				// QTYPE
				0x00, 0x10,
				// QCLASS
				0x00, 0x01,

				// ANSWER
				// NAME (pointer)
				0xc0, 0x0c,
				// TYPE
				0x00, 0x10,
				// CLASS
				0x00, 0x01,
				// TTL
				0x00, 0x00, 0x0e, 0x10,
				// RDLENGTH
				0x00, 0x0f,
				// RDATA
				0x0e, 'F', 'o', 'r', 'm', 'a', 't', ':', ' ', '<', 't', 'e', 'x', 't', '>',

				// ANSWER
				// NAME (pointer)
				0xc0, 0x0c,
				// TYPE
				0x00, 0x10,
				// CLASS
				0x00, 0x01,
				// TTL
				0x00, 0x00, 0x0e, 0x10,
				// RDLENGTH
				0x00, 0x50,
				// RDATA
				0x4f,
				'D', 'e', 's', 'c', 'r', 'i', 'p', 't', 'i', 'v', 'e', ' ',
				't', 'e', 'x', 't', '.', ' ',
				'C', 'o', 'm', 'p', 'l', 'e', 't', 'e', 'l', 'y', ' ',
				'o', 'v', 'e', 'r', 'l', 'o', 'a', 'd', 'e', 'd', ' ',
				'f', 'o', 'r', ' ',
				'a', 'l', 'l', ' ',
				's', 'o', 'r', 't', 's', ' ',
				'o', 'f', ' ',
				't', 'h', 'i', 'n', 'g', 's', '.', ' ',
				'R', 'F', 'C', '1', '0', '3', '5', ' ',
				'(', '1', '9', '8', '7', ')',
			},
			&Message{
				Header: &MessageHeader{
					Id:                    0x227f,
					IsResponse:            true,
					OpCode:                OpCodeStandardQuery,
					IsAuthoritativeAnswer: false,
					IsTruncated:           false,
					IsRecursionDesired:    true,
					IsRecursionAvailable:  true,
					Z:                     0,
					ResponseCode:          ResponseCodeOk,
					QuestionCount:         1,
					AnswerCount:           2,
					NameServerRecordCount: 0,
					AdditionalRecordCount: 0,
				},
				Query: &Query{
					Questions: []*Question{
						{
							Name: &DomainName{Labels: []string{
								"TXT",
								"dns",
								"netmeister",
								"org",
							}},
							Type:  &TypeText,
							Class: &ClassInternet,
						},
					},
				},
				Answers: []*ResourceRecord{
					{
						Name: &DomainName{Labels: []string{
							"TXT",
							"dns",
							"netmeister",
							"org",
						}},
						Type:           &TypeText,
						Class:          &ClassInternet,
						TTL:            0x0e10,
						ResourceLength: 0x0f,
						Data: []byte{
							0x0e, 'F', 'o', 'r', 'm', 'a', 't', ':', ' ', '<', 't', 'e', 'x', 't', '>',
						},
						Resource: nil, // TODO: handle this data type
					},
					{
						Name: &DomainName{Labels: []string{
							"TXT",
							"dns",
							"netmeister",
							"org",
						}},
						Type:           &TypeText,
						Class:          &ClassInternet,
						TTL:            0x0e10,
						ResourceLength: 0x50,
						Data: []byte{
							0x4f,
							'D', 'e', 's', 'c', 'r', 'i', 'p', 't', 'i', 'v', 'e', ' ',
							't', 'e', 'x', 't', '.', ' ',
							'C', 'o', 'm', 'p', 'l', 'e', 't', 'e', 'l', 'y', ' ',
							'o', 'v', 'e', 'r', 'l', 'o', 'a', 'd', 'e', 'd', ' ',
							'f', 'o', 'r', ' ',
							'a', 'l', 'l', ' ',
							's', 'o', 'r', 't', 's', ' ',
							'o', 'f', ' ',
							't', 'h', 'i', 'n', 'g', 's', '.', ' ',
							'R', 'F', 'C', '1', '0', '3', '5', ' ',
							'(', '1', '9', '8', '7', ')',
						},
						Resource: nil, // TODO: handle this data type
					},
				},
			},
		},
	} {
		t.Run(tc.expected.Query.Questions[0].Type.Type, func(t *testing.T) {
			got, err := DeserializeMessage(tc.data)
			require.NoError(t, err)

			assert.Equal(t, tc.expected, got)
		})
	}
}
