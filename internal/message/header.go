package message

import (
	"encoding/binary"
	"errors"
	"fmt"
)

type OpCode uint16

const (
	OpCodeStandardQuery OpCode = iota
	OpCodeInverseQuery
	OpCodeStatus
)

type ResponseCode uint16

const (
	ResponseCodeOk             ResponseCode = 0
	ResponseCodeFormatError    ResponseCode = 1
	ResponseCodeServerFailure  ResponseCode = 2
	ResponseCodeNameError      ResponseCode = 3
	ResponseCodeNotImplemented ResponseCode = 4
	ResponseCodeRefused        ResponseCode = 5
)
const maxResponseCode = uint16(ResponseCodeRefused)

const HeaderByteCount = 12

type HeaderFlag struct {
	mask   uint16
	offset uint8
}

var (
	// top byte
	QR     = HeaderFlag{mask: 0b10000000, offset: 15}
	OPCODE = HeaderFlag{mask: 0b01111000, offset: 11}
	AA     = HeaderFlag{mask: 0b00000100, offset: 10}
	TC     = HeaderFlag{mask: 0b00000010, offset: 9}
	RD     = HeaderFlag{mask: 0b00000001, offset: 8}

	// bottom byte
	Z     = HeaderFlag{mask: 0b01110000, offset: 4}
	RA    = HeaderFlag{mask: 0b10000000, offset: 7}
	RCODE = HeaderFlag{mask: 0b00001111, offset: 0}
)

const (
	OPCODEMax = 15
)

/*
MessageHeader is a header per
https://datatracker.ietf.org/doc/html/rfc1035#section-4.1.1

The header contains the following fields:

	                                1  1  1  1  1  1
	  0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	|                      ID                       |
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	|QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	|                    QDCOUNT                    |
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	|                    ANCOUNT                    |
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	|                    NSCOUNT                    |
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	|                    ARCOUNT                    |
	+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
*/
type MessageHeader struct {
	Id uint16
	// QR
	IsResponse bool
	// OpCode
	OpCode OpCode
	// AA
	IsAuthoritativeAnswer bool
	// IC
	IsTruncated bool
	// RD
	IsRecursionDesired bool
	// RA
	IsRecursionAvailable bool
	// Z
	Z uint8
	// RCODE
	ResponseCode ResponseCode
	// QDCOUNT
	QuestionCount uint16
	// ANCOUNT
	AnswerCount uint16
	// NSCOUNT
	NameServerRecordCount uint16
	// ARCOUNT
	AdditionalRecordCount uint16
}

func (h *MessageHeader) Serialize() []byte {
	// encode in big-endian, per
	// https://datatracker.ietf.org/doc/html/rfc1035#section-2.3.2
	serialized := make([]byte, 0, HeaderByteCount)

	// header
	// https://datatracker.ietf.org/doc/html/rfc1035#section-4.1.1
	serialized = binary.BigEndian.AppendUint16(serialized, h.Id)

	var headerFlags uint16 = uint16(h.OpCode) << OPCODE.offset
	if h.IsResponse {
		headerFlags = 1 << QR.offset
	}
	if h.IsAuthoritativeAnswer {
		headerFlags |= 1 << AA.offset
	}
	if h.IsTruncated {
		headerFlags |= 1 << TC.offset
	}
	if h.IsRecursionDesired {
		headerFlags |= 1 << RD.offset
	}
	// IsRecursionAvailable is for responses only
	// Z is always zero
	// ResponseCode is for responses only

	serialized = binary.BigEndian.AppendUint16(serialized, headerFlags)
	serialized = binary.BigEndian.AppendUint16(serialized, h.QuestionCount)
	serialized = binary.BigEndian.AppendUint16(serialized, h.AnswerCount)
	serialized = binary.BigEndian.AppendUint16(serialized, h.NameServerRecordCount)
	serialized = binary.BigEndian.AppendUint16(serialized, h.AdditionalRecordCount)

	return serialized
}

func DeserializeMessageHeader(data []byte) (*MessageHeader, int, error) {
	if len(data) < HeaderByteCount {
		return nil, 0, fmt.Errorf("header too short, must be %d bytes", HeaderByteCount)
	}

	id := binary.BigEndian.Uint16(data[:2])

	flags := binary.BigEndian.Uint16(data[2:4])

	// top byte
	isResponse := ((flags >> 8) & QR.mask) != 0
	opCode := ((flags >> 8) & OPCODE.mask) >> OPCODE.offset
	if opCode > OPCODEMax {
		return nil, 0, fmt.Errorf("invalid opcode, must be less than %d", OPCODEMax)
	}
	isAuthoritativeAnswer := ((flags >> 8) & AA.mask) != 0
	isTruncated := ((flags >> 8) & TC.mask) != 0
	isRecursionDesired := ((flags >> 8) & RD.mask) != 0

	// bottom byte
	isRecursionAvailable := (flags & RA.mask) != 0
	if (flags&Z.mask)>>Z.offset != 0 {
		return nil, 0, errors.New("invalid reserved bits, must be zero")
	}
	responseCode := (flags & RCODE.mask) >> RCODE.offset
	if responseCode > maxResponseCode {
		return nil, 0, fmt.Errorf("invalid response code, must be less than %d", maxResponseCode)
	}

	return &MessageHeader{
		Id:                    id,
		IsResponse:            isResponse,
		OpCode:                OpCode(opCode),
		IsAuthoritativeAnswer: isAuthoritativeAnswer,
		IsTruncated:           isTruncated,
		IsRecursionDesired:    isRecursionDesired,
		IsRecursionAvailable:  isRecursionAvailable,
		Z:                     0,
		ResponseCode:          ResponseCode(responseCode),
		QuestionCount:         binary.BigEndian.Uint16(data[4:6]),
		AnswerCount:           binary.BigEndian.Uint16(data[6:8]),
		NameServerRecordCount: binary.BigEndian.Uint16(data[8:10]),
		AdditionalRecordCount: binary.BigEndian.Uint16(data[10:12]),
	}, HeaderByteCount, nil
}
