package message

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestDeserializeDomainName(t *testing.T) {
	for _, tc := range []struct {
		desc           string
		data           []byte
		nameMap        namePointerMap
		offset         int
		expectedName   *DomainName
		expectedLength int
	}{
		{
			"sequence of labels",
			[]byte{
				0x07, 'e', 'x', 'a', 'm', 'p', 'l', 'e',
				0x03, 'c', 'o', 'm', 0x00,
			},
			namePointerMap{},
			0,
			&DomainName{Labels: []string{"example", "com"}},
			13,
		},
		{
			"pointer to label",
			[]byte{0xc0, 0x10},
			namePointerMap{
				0x10: lookupInfo{
					name:   &DomainName{Labels: []string{"example", "com"}},
					length: 11,
				},
			},
			0,
			&DomainName{Labels: []string{"example", "com"}},
			2,
		},
		{
			"labels with pointer",
			[]byte{
				0x01, 'a', 0x01, 'b', 0xc0, 0x1f,
			},
			namePointerMap{
				0x1f: lookupInfo{
					name:   &DomainName{Labels: []string{"example", "com"}},
					length: 11,
				},
			},
			0,
			&DomainName{Labels: []string{"a", "b", "example", "com"}},
			6,
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			got, length, err := DeserializeDomainName(tc.data, tc.offset, tc.nameMap)
			require.NoError(t, err)

			require.Equal(t, tc.expectedName, got)
			require.Equal(t, tc.expectedLength, length)
		})
	}
}

func TestDeserializeDomainName_InvalidLabel(t *testing.T) {
	for _, tc := range []struct {
		data        []byte
		expectedErr string
	}{
		{
			[]byte{
				0x07, 'e', 'x', 'a', 'm', 'p', 'l', 'e',
				0x10, 'c', 'o', 'm', 0x00,
			},
			"failed reading label: character string length of out bounds of data",
		},
	} {
		t.Run(tc.expectedErr, func(t *testing.T) {
			_, _, err := DeserializeDomainName(tc.data, 0, namePointerMap{})
			require.EqualError(t, err, tc.expectedErr)
		})
	}
}
