package message

import (
	"fmt"
)

type Message struct {
	Header  *MessageHeader
	Query   *Query
	Answers []*ResourceRecord
}

func DeserializeMessage(data []byte) (*Message, error) {
	header, length, err := DeserializeMessageHeader(data)
	if err != nil {
		return nil, fmt.Errorf("failed to deserializer header: %v", err)
	}
	offset := length

	questionCount := header.QuestionCount
	questions := make([]*Question, questionCount)
	nameMap := namePointerMap{}
	for i := 0; i < int(questionCount); i++ {
		question, length, err := DeserializeQuestion(data[offset:], offset, nameMap)
		if err != nil {
			return nil, fmt.Errorf("failed to deserialize question #%d: %v", i+1, err)
		}
		offset += length
		questions[i] = question
	}
	query := &Query{Questions: questions}

	answerCount := header.AnswerCount
	answers := make([]*ResourceRecord, answerCount)
	for i := 0; i < int(answerCount); i++ {
		resource, length, err := DeserializeResourceRecord(data[offset:], offset, nameMap)
		if err != nil {
			return nil, fmt.Errorf("failed to deserialize answer #%d: %v", i+1, err)
		}
		offset += length
		answers[i] = resource
	}

	return &Message{Header: header, Query: query, Answers: answers}, nil
}
