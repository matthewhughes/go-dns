package message

import (
	"errors"
)

func DeserializeCharacterString(data []byte) (string, int, error) {
	length := int(data[0])
	if len(data) < length {
		return "", 0, errors.New("character string length of out bounds of data")
	}

	return string(data[1 : length+1]), length + 1, nil
}
