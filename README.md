# `go-dns`

A DNS client (implementing only
[RFC 1035](https://www.rfc-editor.org/rfc/rfc1035)) it is meant purely as a
learning exercise.
