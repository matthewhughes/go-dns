package main

import (
	"bufio"
	"fmt"
	"log"
	"net"

	"gitlab.com/matthewhughes/go-dns/internal/message"
)

func main() {
	query, err := message.NewQuery(
		"example.com",
		message.TypeAuthoritativeNameServer,
		message.ClassInternet,
	)
	if err != nil {
		log.Fatalf("failed to build query: %v", err)
	}

	conn, err := net.Dial("udp", "1.1.1.1:53")
	if err != nil {
		log.Fatalf("failed to dial: %v", err)
	}
	defer conn.Close()

	req := query.Serialize()
	fmt.Print("sending: ")
	for _, b := range req {
		fmt.Printf("0x%02x ", b)
	}
	fmt.Println()

	n, err := conn.Write(req)
	if err != nil {
		log.Fatalf("failed to write to socket: %v", err)
	}
	log.Printf("wrote %d bytes", n)

	buf := make([]byte, 512)
	n, err = bufio.NewReader(conn).Read(buf)
	if err != nil {
		log.Fatalf("failed to read bytes: %v", err)
	}
	fmt.Printf("read %d bytes\n", n)
	fmt.Print("header: ")
	printData(buf[:message.HeaderByteCount])

	header, _, err := message.DeserializeMessageHeader(buf[:n])
	if err != nil {
		log.Fatalf("failed to deserialize header: %v", err)
	}
	fmt.Printf("%+v\n", *header)

	fmt.Print("resources :")
	printData(buf[message.HeaderByteCount:n])
	//resource, _, err := message.DeserializeResourceRecord(buf[message.HeaderByteCount:n])
	//if err != nil {
	//	log.Fatalf("failed to deserialize record: %v", err)
	//}
	//fmt.Printf("%+v\n", *resource)
}

func printData(buf []byte) {
	for _, b := range buf {
		fmt.Printf("0x%02x ", b)
	}
	fmt.Println()
}
