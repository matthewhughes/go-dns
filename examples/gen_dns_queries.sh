#!/usr/bin/env bash

set -o errexit -o pipefail -o nounset

TYPES=(
    "A"
    "NS"
    "MD"
    "MF"
    "CNAME"
    "SOA"
    "MB"
    "MG"
    "MR"
    "NULL"
    "WKS"
    "PTR"
    "HINFO"
    "MINFO"
    "MX"
    "TXT"
    "AXFR"
    "MAILB"
    "MAILA"
)

for t in "${TYPES[@]}"
do
    dig +noedns @8.8.8.8 $t.dns.netmeister.org $t
done
