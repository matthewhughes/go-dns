#!/usr/bin/env bash

set -o errexit -o pipefail -o nounset

if [ $# != 1 ]
then
    echo "usage: $0 <query-pcap-file>" >&2
    exit 1
fi

pcap_json="$(
    tshark \
        -e udp.payload \
        -e dns.qry.type \
        -T json \
        -r "$1"  \
        -- dns and ip.src == 8.8.8.8
)"

while read -r line
do
    payload="$(cut --delimiter ' ' --fields 1 <<< "$line")"
    query_type="$(cut --delimiter ' ' --fields 2 <<< "$line")"

    printf -- '-----\nQuery type %d\n-----\n' "$query_type"
    xxd -r -p <<< "$payload" | xxd
done < <(jq --raw-output '
    .[]._source.layers |
    select(."udp.payload"[0] != null) |
    "\(."udp.payload"[0]) \(."dns.qry.type"[0])"' <<< "$pcap_json"
)
